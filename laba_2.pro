/*****************************************************************************

		Copyright (c) My Company

 Project:  LABA1_1
 FileName: LABA_2.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "laba1_1.inc"

domains
	list=integer*
	number=integer

predicates
	%subsum(list, number, list, integer)
	subsum_2(list, number, list)
	%sum_list(list, number)
	%eq(number, number)
	%gen(integer)
	member(integer, list)

clauses
	%sum_list([], 0) :- !.
	%sum_list([Head|Tail], Sum) :- sum_list(Tail, TailSum), Sum = TailSum + Head.
	%subsum([], Numb, Answ) :- !.
	/*subsum([], 0, [], X) :- !.
	subsum(TheList, Numb, Answ, Temp):- Temp >= 0, sum_list([Temp|TheList], X),
					X = Numb, Answ = [Temp|TheList].
	subsum([Head|Tail], Numb, Answ, Temp):- subsum(Tail, Numb, Answ, Head).
	subsum([Head|Tail], Numb, Answ, Temp):- subsum(Tail, Numb, Answ, Temp).
		*/
	member(Elem, [Elem|_Tail]).
	member(Elem, [_Head| Tail]):- member(Elem, Tail).
			
	subsum_2([], 0, []).
	subsum_2([H|Tail],N,[H|SubTail]) if N1=N-H,subsum_2(Tail,N1,SubTail).
	subsum_2([_|Tail],N,Sub):-subsum_2(Tail,N,Sub).

goal
	%sum_list([1,2,3,4], X).
	%subsum([1,2,5,3,2], 5, Sub, -1).
	subsum_2([1,2,5,3,2], 5, Sub).

			

