/*****************************************************************************

		Copyright (c) My Company

 Project:  LABA1_1
 FileName: LABA_3.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "laba1_1.inc"
%include "laba1_1.con"
%include "hlptopic.con"


%domains

	
database -graphs
	nondeterm edge(char, char, unigned)	

%facts - graph
%	edge : (char, chat, unsigned).
predicates
	path(char, % Start node [In]
	    char, % Stop node [In]
	    char*, % passed node list [In] 
	    char*, % from start to stop node list
	    unsigned, % sum passed distandce
	    unsigned) % from start to stop sum distance


clauses
	edge('a', 'b', 60).   edge('b', 'a', 60).
	edge('a', 'c', 15).  edge('c', 'a', 15).
	edge('a', 'd', 70).  edge('d', 'a', 70).
	edge('b', 'c', 50). edge('c', 'b', 50).
 

goal


