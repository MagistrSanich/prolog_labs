include "laba1_1.inc"

domains
    list=integer*
    lists=list*
    
predicates
   list_absorber(list,lists)
   
clauses
    list_absorber([X1],[[X1]]):-!.
    list_absorber([X1|X2],[[X1]|R]):-list_absorber(X2,R).
    
goal
    list_absorber([1,2,3,10,6],X).   	


/*include "laba1_1.inc"
predicates
  parent(String, String)
  male(String)
  female(String)
  brother(String, String)
  print

clauses
  parent("Tom","Jake").
  parent("Janna","Jake").
  parent("Tom","Tim").
  parent("Janna","Tim").
  male("Tom").
  male("Tim").
  male("Jake").
  female("Janna").

  brother(X,Y):-parent(Z,X),parent(Z,Y),male(X),X<>Y.
  print:-brother(X,Y),write(X,"- brother -",Y),nl,fail.*/

%goal
%  write("hello").
/*
include "laba1_1.inc"

predicates

  %laba1_1()
  %eats(String, String)
  p([], []).

clauses

  %laba1_1():-!.
  %eats("fred", "oranges"). 
  p([H|T1], [[H]|T2]) :- p(T1, T2).

goal

  %laba1_1().
  %write("hello world").
  %?- eats("fred", "oranges").
*/
%include "laba1_1.inc"
%p([], []).
%p([H|T1], [[H]|T2]) :- p(T1, T2).
